/**
 * Created by akaramya on 11/7/2016.
 */
var gulp = require('gulp');
var paths = {
    scripts: [
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/jquery/dist/jquery.js',
        'node_modules/jquery.rateit/scripts/jquery.rateit.js',
        'node_modules/datatables.net-bs/js/dataTables.bootstrap.js',
        'node_modules/dropzone/dist/dropzone.js'
    ],
    styles: [
        'node_modules/bootstrap/dist/css/bootstrap.css',
        'node_modules/font-awesome/css/font-awesome.css',
        'node_modules/jquery.rateit/scripts/rateit.css',
        'node_modules/datatables.net-bs/css/dataTables.bootstrap.css',
        'node_modules/dropzone/dist/dropzone.css'
    ],
    fonts: ['node_modules/font-awesome/fonts/**']
};

gulp.task('lib_scripts', function() {
    return gulp.src(paths.scripts)
        .pipe(gulp.dest('app/assets/js'));
});

gulp.task('lib_styles', function() {
    return gulp.src(paths.styles)
        .pipe(gulp.dest('app/assets/css'));
});

gulp.task('lib_fonts', function() {
    return gulp.src(paths.fonts)
        .pipe(gulp.dest('app/assets/fonts'));
});

gulp.task('watch', function() {
    gulp.watch(paths.scripts, ['lib_scripts']);
    gulp.watch(paths.styles, ['lib_styles']);
    gulp.watch(paths.styles, ['lib_fonts']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'lib_scripts', 'lib_styles', 'lib_fonts']);