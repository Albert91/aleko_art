/**
 * Created by Albert on 11/12/2016.
 */
app.use(function(req, res, next) {
    res.locals.base_url = 'http://localhost:3030';
    res.locals.admin_base_url = 'http://localhost:3030/admin';
    res.locals.menu_items = [
        {
            title: "Categories",
            slug: 'categories',
            icon: "fa fa-user"
        },
        {
            title: "Items",
            slug: 'items',
            icon: "fa fa-futbol-o"
        },
        {
            title: "About",
            slug: 'about',
            icon: "fa fa-dashboard"
        }
    ]
    next();
})