var express = require('express');
var router = express.Router();
Categies = require('../../models/categories.js');

/* GET games listing. */
router.get('/', function(req, res, next) {
  var categories = [{
      _id: "1",
      category_name: "Ceramics",
      logo: ""
  },{
      _id: "1",
      category_name: "Sculptures",
      logo: ""
  }];
  res.render('admin/categories/index', { title: 'Categories', categories: categories});
})

router.get('/new', function(req, res, next) {
   res.render('admin/categories/new', { title: 'New category', category: []});
});

router.get('/:id', function(req, res, next) {
   res.render('admin/categories/edit', { title: 'Edit category', category: []});
})

module.exports = router;
