/**
 * Created by Albert on 11/19/2016.
 */
var express = require('express');
var router = express.Router();
var Items = require('../../models/items.js');

/* GET games listing. */
router.get('/', function(req, res, next) {
     res.render('admin/items/index', { title: 'Items', items: []});
});

router.get('/new', function(req, res, next) {
     res.render('admin/items/new', { title: 'New Item', item: []});
});

module.exports = router;