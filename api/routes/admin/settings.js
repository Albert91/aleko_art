/**
 * Created by akaramya on 11/8/2016.
 */
var express = require('express');
var router = express.Router();

/* GET games listing. */
router.get('/', function(req, res, next) {
    res.render('admin/settings', { title: 'Settings'});
});

module.exports = router;