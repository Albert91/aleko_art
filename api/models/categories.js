/**
 * Created by Albert on 11/12/2016.
 */
// The Category model

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var categoriesSchema = new Schema({
    _id: String,
    name: String
});

module.exports = mongoose.model('games', categoriesSchema);