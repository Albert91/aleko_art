/**
 * Created by Albert on 11/13/2016.
 */
// The Careers model

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var itemsSchema = new Schema({
    _id: String,
    career_name: String
});

module.exports = mongoose.model('careers', itemsSchema);