/**
 * Created by akaramya on 11/7/2016.
 */
var home = require('./api/routes/index');

var login = require('./api/routes/admin/login');
var index = require('./api/routes/admin/index');
var categories = require('./api/routes/admin/categories');
var items = require('./api/routes/admin/items');
var settings = require('./api/routes/admin/settings');

app.use('/', home);

app.use('/login', login);
app.use('/admin', index);
app.use('/admin/categories', categories);
app.use('/admin/items', items);
app.use('/admin/settings', settings);